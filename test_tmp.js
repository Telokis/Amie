'use strict';

const amie = require('./index.js');
const path = require('path');

amie({
    input: path.join(__dirname, '..', '/test_cpp/main.cpp')
}, (err, result) => {
    console.log(err);
    console.log(result);
});