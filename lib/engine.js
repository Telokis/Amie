'use strict';

const fs = require('fs');
const path = require('path');
const async = require('async');

let amie = {
    includeRegex: /^[ \t]*#[ \t]*include[ \t]/,
    systemIncludeRegex: /<(.+?)>/,
    userIncludeRegex: /"(.+?)"/
};

/**
 * Will process a single line of code. Effectively handling includes.
 * 
 * @param {String} root       Directory of the current folder. For relative includes.
 * @param {String} line       Current line to process.
 * @param {Function} callback Callback to to call with the new line.
 */
amie.processLine = function(root, line, callback) {
    if (amie.includeRegex.test(line)) {
        const match = amie.userIncludeRegex.exec(line);
        if (!match)
            return callback(null, line);
        const file = path.resolve(root, match[1]);
        amie.doFile(file, callback);
    } else {
        callback(null, line);
    }
};

/**
 * Amalgamate a single file.
 * 
 * @param {String}   inputFile Path to the file to amalgamate.
 * @param {Function} callback  The callback to call once the file is done.
 */
amie.doFile = function(inputFile, callback) {
    fs.readFile(inputFile, 'utf8', (err, data) => {
        if (err)
            return callback(err);
        let lines = data.split('\n');
        async.map(lines, async.apply(amie.processLine, path.dirname(inputFile)),
            (err, res) => callback(err, res ? res.join('\n') : undefined));
    });
};

/**
 * Main function used to amalgamate stuff.
 * 
 * @param {Object}   options  Object defining the options for the engine.
 * @param {Function} callback The callback to call once amalgamated.
 */
amie.amalgamate = function(options, callback) {
    const input = options.input;

    amie.doFile(input, callback);
};

module.exports = amie.amalgamate;